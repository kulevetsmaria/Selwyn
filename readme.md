This is a repository of all my MLPFIM vectors, ordered as I have them on my computer.

* The **Cutie Marks** folder is what it says on the tin; a separate readme in that folder details its contents. **Episodic Traces** is also self-explanatory.
* **RCC** contains the pictures done with others in mind: requests, commissions, collaborations and trades. These are arranged by alphabetical order on file name.
* **Resources** contains building blocks for larger vectors that aren't cutie marks, vectoring tutorials, the iconography I use for my various web presences and other things.

The remaining two top-level folders are a little trickier.

* If the vector contains a character who is in some way transformed (e.g. is a genie, as on my [Albumin Flask](https://albuminflask.tumblr.com) or [Ain't Never Had Friends Like Us](http://aintneverhadfriendslikeus.tumblr.com) Tumblr blogs), it goes into the **Transformations** folder, which has some sub-folders for the type of transformation undergone.
* Otherwise, it goes into the miscellaneous **Standalones** folder, which has sub-folders for pictures of my OC [Parcly Taxel](https://parclytaxel.tumblr.com/rf) among other things.

The repository itself is named after the [Cambridge college](http://www.sel.cam.ac.uk). Some of the files have been compressed with my SVG scouring script [Rarify](https://github.com/Parclytaxel/Kinross/blob/master/rarify.py) and should be opened/saved in Inkscape before showing elsewhere. I also use a number of free fonts not normally found installed on a computer; these can be obtained from [The League of Moveable Type](https://theleagueofmoveabletype.com), the [Open Font Library](https://fontlibrary.org) and Google Fonts, with the exception of my very own font _Tantalum_ which can be obtained [here](https://drive.google.com/open?id=0B9XBCp7FlThxYVRhYVM2M3Rrbjg).
